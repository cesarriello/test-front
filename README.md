# Test de front

Construir uma SPA de lista de tarefas (To-Do List).

Dicas:

- Trabalhe pensando em:

  - Reutilização de código;

  - Adoção de padrões de projeto;


- Não é obrigado a seguir a risca as sugestões ou as telas propostas;

- Levamos tudo em conta: de README a commits.

- Divirta-se, construa soluções que sejam a sua cara.

- Quando terminar, criar um repositório no github ou gitlab.


#### Funcionalidades

Não há restrições quanto a plugins ou frameworks para implementação de CSS ou JS, mas você ganha um bônus se usar o Angular.


#### SPA de listagem de tarefas

- Criação de um filtro de tarefas realizadas ou não realizadas;

- Botão de cadastro de uma nova tarefa;

- Botão de remoção da tarefa;

- Botão para editar tarefa;

- Modal (ou qualquer outra solução personalizada) para edição de tarefa;

Sugestão de atributos para um objeto _Tarefa_

- Descrição;

- Data e hora que a tarefa acontecerá;


#### Finalizando

Assim que terminar envie um e-mail com o endereço do seu repo para:

- cesar.muoio@itau-unibanco.com.br e renato.fontes@itau-unibanco.com.br
